module Main where

import Language.Abs
import Language.Lex 
import Language.Par
import Language.Print

import Data.Text ()
import qualified Data.Text as T

import System.IO 

import Lib

main :: IO ()
main = do
    putStrLn "enter next term"
    hFlush stdout 
    a <- getLine
    let texta = T.pack a -- this is the text
    let tokens = myLexer texta -- [Token]
    case pTerm tokens of 
      Left s -> putStrLn s 
      Right te -> putStrLn $ printTree te

    putStrLn "done"

