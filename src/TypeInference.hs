{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE FlexibleContexts  #-}

module TypeInference where

import Control.Unification
import Control.Unification.IntVar
import Control.Unification.Types
import Control.Monad.Identity
import Control.Monad.Trans
import Control.Monad.State.Strict
import Control.Monad.Except

import Data.Text (Text)
import qualified Data.Text as Text (pack)
import qualified Data.List
import qualified Data.Map as M

import Bound

import Language.Abs

{-
IGNORE NOTE:
You may safely ignore everything from this point to the next "IGNORE NOTE" point.
This contains the code required to bootstrap the Unification-fd library for our language of types.

However, it may be helpful to know a little about what's happening behind the sences.  Since our language is polymorphic, it may contain 
type variables.  The language of types may be specified 
by a data type as follows:
    data Type a = 
        TR
        | Unit 
        | Pair (Type a) (Type a)
        | TypeVar a

However, in order to perform syntactic unification of types as terms, they are required to be an "absolutely free" or "anarchic" algebra aka a "term algebra"  In Haskell, this is enforced 
by requiring the language of types that we perform unification on to be constructed as a free monad.  Note the shape of the constructors of Type a.  There is a single constructor TypeVar a
with a non-recursive use of a, and the other uses of a are only recursive within TPair.  This means that if we define 

    data TypeFormal b = 
        R 
        | TUnit 
        | TPair b b 

Then we can almost recover type by setting Type a = TypeFormal (TypeFormal a); however, we'd be missing the TypeVar.  The constructor UTerm does this and defines a Var constructor for us.  Formally, UTerm is defined by 
    data UTerm f a =
        UTerm (f (UTerm f a))
        | UVar a

If we then consider UTerm TypeFormal a we wee 
    data UTerm TypeFormal a 
    == 
        UTerm R 
        | UTerm TUnit 
        | UTerm (TPair (UTerm TypeFormal a) (UTerm TypeFormal a))
        | UVar a
Which we can clearly convert back and forth to the original Type without loss. 
-}


data TypeFormal a =
    R
    | TUnit
    | TPair a a
    deriving (Show,Eq,Functor,Foldable,Traversable)

type Type = UTerm TypeFormal IntVar
type IntBinding = IntBindingT TypeFormal Identity
type TypingFailure = UFailure TypeFormal IntVar

{-
When performing type inference, we record types of variables and functions as type schemes.  Variables that come from function inputs and let bindings 
will have all their type variables abstracted in the type scheme, and variables that come from differentiation will have their variables completely open.
Note that the tutorial language lacks let-bindings, function declarations, and derivatives, so the use of a type scheme is overkill -- however, it will be 
useful in the real language.  Also, the use of the FunTypeEnv here is un-needed since the tutorial language has no function declarations.  However, again, 
the setup here should be usable in the real language.

A type scheme is a way to specify a generic type while binding the type variables so that only the shape of the type matters.  For example, we write 
    Forall a,b. (a,(a,b))
To indicate a tuple whose second element is a tuple.  By binding the variables, the above type is exactly the same as 
    Forall c,r. (c,(c,r))
Note, this is done implicitly in some languages.  For example, in Haskell, one would simply write 
    (a,(a,b))
And the Forall a,b is silently added to the front of the type scheme.  This works because the quantifiers all appear at the outermost level (though Haskell does support quantifiers at deeper levels -- these have to then be explicitly marked).

Since we are dealing with type schemes, which have bound variables, we use the Bound library to keep track of the bound variables.  Without getting into details, the bound library allows 
us to specify that we are treating 
    Forall a,b. (a,(a,b))
as a bound type.  Then, all wee need to record is the number of variables, and then we use the scope bit to allow us to make a sort of type-level function that can be evaluated to 
obtain an actual type.  In the bound library, Forall a,b. (a,(a,b)) is essentially represented as 
    Forall 2 (\x y.(x,(x,y)))
In fact we can take the type (x,(x,y)) and abstract out its variables to obtain the scope -- that is we can get the internal representation as 
    Forall 2 (abstract x y ((x,(x,y))))
And then we can instantiate this with actual types, a,b with 
    instantiate [a,b] (\x y.(x,(x,y))) 
To obtain the concrete type 
    (a,(a,b))
The above description isn't exactly accurate, as we've oversimplified a few details of how the library works in practice.
However, programmatically, it works out pretty simply.  Ask if you have any questions about how to use the library.  You can also find a more full-featured 
    implementation in the simple-functional-lang project on this gitlab repository.
-}
data Scheme a = Forall Int (Scope Int (UTerm TypeFormal) a)
type VarTypeEnv a = M.Map Text (Scheme a)
type FunTypeEnv a = M.Map Text (Scheme ([a],a))
type TypeEnv a = (VarTypeEnv a, FunTypeEnv a)

-- Utilities for constructing types as Type
pair :: UTerm TypeFormal v -> UTerm TypeFormal v -> UTerm TypeFormal v
pair m n = UTerm $ TPair m n
unit :: UTerm TypeFormal v
unit = UTerm TUnit
real :: UTerm TypeFormal v
real = UTerm R


{-
This instance declaration provides all the real work in the unification.  All we need to do is describe how 
to match TypeFormal (the non-recursive datatype), and the matching of arbitrary UTerms is handled for us, along 
with the unification.
-}
instance Unifiable TypeFormal where
    zipMatch R R = Just R
    zipMatch TUnit TUnit = Just TUnit
    zipMatch (TPair l1 r1) (TPair l2 r2) = Just $ TPair (Right (l1,l2)) (Right (r1,r2))
    zipMatch _ _ = Nothing

----------------------- Some convenience functions ----------------------------


-- Convenience function for getting a fresh variable in the binding monad
getFreshVar :: (MonadTrans t1,BindingMonad t2 v m) => t1 m (UTerm t2 v)
getFreshVar = lift $ UVar <$> freeVar

{-
streamFreshVars returns a list of freshvariables of some specified length.  This is useful for binding a bunch of variables in one go.

Warning!  At first thought, you might think you could just do 
    streamFreshVars = mapM (const getFreshVar) [1 ..]
However, this will always diverge -- you cannot get an infinite stream using lists in the monad stack we are using -- the reason 
is that some of the binds will force a strict evaluation.

Or in other words, Haskell is merely lazy, not **lazy**. 
-}
streamFreshVars :: (MonadTrans t1,BindingMonad t2 v m,Monad (t1 m)) => Int -> t1 m [UTerm t2 v]
streamFreshVars n = mapM (const getFreshVar) [1 ..n]

{-
fullAbstractType takes in a type and abstracts out every variable in it, and thus creates a closed scheme.
Warning: one should only use this function outside of the monad -- that is the type needs to have all its bindings 
resolved.  We thus use our own free variable function. 
-}
fullyAbstractType :: (Eq a) => UTerm TypeFormal a -> Scheme a
fullyAbstractType ty =
    let (size,theFreeVars) = freeVarsTypeList ty
    {-
    By construction length theFreeVars == size.  Hence if size == 0, then listToMap theFreeVars is (extensionally) equal to const Nothing.
    Then in fullInstantiateWithFresh, we will see the case EQ.  Then any time listToMap theFreeVars x returns Just _, x will be instantiated 
    with ().  However, listToMap theFreeVars is const Nothing, so the case of EQ will project "ty" as given here.
    In other words fullInstantiationWithFresh (fullyAbstractType ty) == ty whenever size == 0.
    -}
    in Forall size $ abstract (listToMap theFreeVars) ty

{-
When we lookup a variable in our environment, we get back a type scheme.  To get a concrete type to assign the variable, we must instantiate the type,
and to do so, we must instantiate any bound variables with fresh variables.  fullInstantiateWithFresh fully instantiates a type scheme with fresh variables.
-}
fullInstantiateWithFresh :: (MonadTrans t1, BindingMonad TypeFormal a m, Monad (t1 m))
    => Scheme a ->  t1 m (Maybe (UTerm TypeFormal a))
fullInstantiateWithFresh (Forall n sc) = case compare 0 n of
    EQ -> return $ Just $ instantiate (const (UTerm TUnit)) sc -- it's a type with no bound variables, so we just project the type out
    LT -> do -- this is the case that we have Forall a0,...,a_{n-1}. ty
        streamedFresh <- streamFreshVars n
        let freshiesNeeded = take n streamedFresh
        return $ Just $ instantiate (freshiesNeeded !!) sc
    GT -> return Nothing -- if n < 0 , that would mean the scheme has negatively many bound variables...

{-
This convenience function allows us to pull the type of a symbol out of the environment and freshen up its variables.
Note: this works only for variable contexts!  You will need to implement a similar function to retrieve and freshen 
the type scheme of a function variable.
-}
getFreshedTyEnv :: (Ord k, MonadTrans t1, BindingMonad TypeFormal a m, Monad (t1 m)) =>
    k -> M.Map k (Scheme a) -> t1 m (Maybe (UTerm TypeFormal a))
getFreshedTyEnv name tyEnv = case M.lookup name tyEnv of
    Nothing -> return Nothing
    Just scheme -> fullInstantiateWithFresh scheme


{-
This function should remind you why you love functional programming.
elemIndex :: Eq a => a -> [a] -> Maybe Int 
which "searches" for an element in a list may be equally regarded by flipping the arguments around 
as 
"elemIndex" :: Eq a => [a] -> a -> Maybe Int
which takes in a list and gives you a characteristic function for the list.
-}
listToMap :: Eq a => [a] -> a -> Maybe Int
listToMap = flip Data.List.elemIndex

{-
Collect the free variables of a type from left to right
 -}
-- freeVarsType :: Ord a => UTerm TypeF a -> S.Set a
-- freeVarsType t = case t of
--   UVar a -> S.singleton a
--   UTerm tf -> case tf of
--     R -> S.empty
--     TUnit -> S.empty
--     TPair ut ut' -> S.union (freeVarsType ut) (freeVarsType ut')
--     TArr ut ut' -> S.union (freeVarsType ut) (freeVarsType ut')

-- {-
-- Since getting the size of a set is O(1) we prefer to get the 
-- number of free variables before converting to a list
-- -}
-- freeVarsTypeList :: Ord a => UTerm TypeF a -> (Int,[a])
-- freeVarsTypeList ty =
--     let theVars = freeVarsType ty
--     in (S.size theVars,S.toList theVars)


freeVarsTypeList :: UTerm TypeFormal a -> (Int,[a])
freeVarsTypeList ty = freeVarsTypeListAccum ty 0 []

freeVarsTypeListAccum :: Num t => UTerm TypeFormal a -> t -> [a] -> (t, [a])
freeVarsTypeListAccum ty sizeAcc varAcc = case ty of
    UVar a -> (sizeAcc+1,a:varAcc)
    UTerm R -> (sizeAcc,varAcc)
    UTerm TUnit -> (sizeAcc,varAcc)
    UTerm (TPair a b) ->
        let (newSize,newAcc) = freeVarsTypeListAccum b sizeAcc varAcc
        in freeVarsTypeListAccum a newSize newAcc

{-
IGNORE NOTE:
At this point, you should pay attention again.  This contains the actual code that needs to be filled in to implement type inferencing.
-}


{-
As a brief of lesson in the monad transformer library -- we can stack monad transformers to pipe multiple monadic effects through.
Let's walk through this particular monad transformer stack and how to use it in practice.

At the top-level we are working with unification.  Thus there are two things that can happen -- a unification failure or a successful unification of Type.
So at the top level we have ExceptT TypingFailure (Blah blah blah) Type
This says that either unification failed, resulting in a TypingFailure (see above) or it passed and the result is a unification type, that results in a Blah blah blah of Type.
Of course the Blah blah blah needs to be something to do with unification.

Blah blah blah is where the unification of Type happens.  Notice that at the top level there is IntBindingT TypeFormal (MORE_BLAH).  The IntBindingT is what threads all the substitutions 
collected during unification at the top-level.  We will see later how to apply all these bindings.  The way they are stored is to perform optimal unification, and a bunch of stuff
happens behind the scenes, so we don't touch the actual bindings until the very end when we actually substitute them into our type.  Thus, we never interact with this level of the 
monad stack.

The MORE_BLAH is where we start to get things we can use.  Note that this is of the shape ExceptT String (EVEN_MORE_BLAH).  
The important bit is the ExceptT String.  This says that we have an inner computation that can fail with an error message, recorded as a string, or succeed with the EVEN_MORE_BLAH applied.
Okay, so what strings are we using here...  Here we are using strings for variable not-in-scope errors.  An improvement to this would be to create a newtype for out-of-scope errors, as 
raw strings don't encode their intent.  This is left as an exercise for the willing.  Anyways, to access this we use lift $ lift $ stuff-in-exceptT.  There is really only one useful method
for us in ExceptT, so realistically our interactions are always of the form lift $ lift $ throwError (errorMsg_as_a_string) -- we need to lift twice because we're at the second 
level in the stack of monad transformers.  You can see this in inferTerm for the variable case, where a 
lift $ lift $ throwError ("Variable:" ++ show x ++ "not in scope") is used.

The EVEN_MORE_BLAH is the innermost monadtransformer we have access to.  It is StateT (TypeEnv IntVar) m.  Nevermind the m -- for our purposes it will usually get filled in with Identity 
or possibly LogicT Identity in the future.  We use this to store the state -- where our state is type of every variable known so far.  There are three methods for StateT that we will use 
in practice.  get, set, and modify.  Since StateT is at third level deep in the stack of monad transformers we have to use three lifts.
    cur <- lift $ lift $ lift $ get 
will create a new variable called cur and put the current state in it 
    lift $ lift $ lift $ set newSt
will overwrite the existing state with newSt (provided newSt has been previously defined).  Note, newSt cannot change the type of the state -- in our case the state is an environment of variables with their types.
and 
    lift $ lift $ lift $ modify f
will modify the existing state using a function f : TypeEnv IntVar -> TypeEnv IntVar.  For instance 
    lift $ lift $ lift $ modify (M.insert newVar newType) 
will update the type environment to contain newVar whose type is newType (provided newVar,newType are previously defined).

An example of state manipulation can be seen in inferTerm in the variable case.  We have to retrieve the type environment to look in it and see what type x has
    tyEnv <- lift $ lift $ lift $ get
-}
-- inferTerm :: (Monad m)
--     => Term
--     -> Type 
--     -> ExceptT TypingFailure (IntBindingT TypeFormal (ExceptT String (StateT (TypeEnv IntVar) m) )) Type
inferTerm :: (Monad m) => Term' a -> Type ->  ExceptT TypingFailure (IntBindingT TypeFormal (ExceptT String (StateT (TypeEnv IntVar) m))) Type
inferTerm term ty = case term of
    Var _ x -> do
        tyEnv <- lift $ lift $ lift (gets fst) -- get the variable type environment
        xTy <- getFreshedTyEnv (pIdentToText x) tyEnv -- obtain the type of x from the environment with variables freshened
        case xTy of
            Nothing -> lift $ lift $ throwError ("Variable: " ++ show x ++ " is not in scope") -- if the variable is not found, throw an error
            Just xTyActual -> do 
                xTyActual =:= ty -- otherwise inferTerm term ty holds exactly if the type found in the environment is unifiable with ty
    Const _ n -> do
        ty =:= UTerm R -- the type of a constant forces ty and R to be unifiable
    Cos _ m -> do 
        mTyV <- getFreshVar
        mTy <- inferTerm m mTyV -- get the type of m
        mTy =:= UTerm R -- m must be a real
        ty =:= UTerm R -- the output must be real
    Sin _ m -> do -- sin is the same as cos
        mTyV <- getFreshVar 
        mTy <- inferTerm m mTyV
        mTy =:= UTerm R
        ty =:= UTerm R
    Times _ m n -> do -- in this language we have only real multiplication, so both m,n must be real, ty must be real
        mTyV <- getFreshVar 
        nTyV <- getFreshVar 
        mTy <- inferTerm m mTyV
        nTy <- inferTerm n nTyV
        mTy =:= UTerm R
        nTy =:= UTerm R
        ty =:= UTerm R
    Plus _ m n -> do -- in this language every type has a plus, yet only things of the same type may be added
        a <- getFreshVar 
        b <- getFreshVar 
        mTy <- inferTerm m a 
        nTy <- inferTerm n b 
        mTy =:= nTy 
        ty =:= nTy




pIdentToText :: PIdent -> Text
pIdentToText (PIdent ((r,c),x)) = x




------------ In the remainder of this file, we unpack and unwrap the above computation --------------------------

{-
We want to know, given a term m, does there exist a type q with m : q 
-}
inferQuery term = do 
    q <- getFreshVar 
    inferTerm term q

{-
In the unification monad, the bindings are not applied in step for efficiency reasons.  
And we want to delay apply the bindings as long as possible.  inferAndApply is really 
just an exit point for the type inference.
-}
inferAndApply term = do
    the_ty <- inferQuery term
    applyBindings the_ty


{-}
We have a stack of four monad transformers that we wish to now unwrap to obtain a type.
We will discard the the typing context, the bindings, and condense down to just 
having knowledge of the exceptions or the resulting type.
 ExceptT TypingFailure (IntBindingT TypeF (ExceptT String (StateT Env m))) Type
-}
unwrapTypeMT :: s -> ExceptT e1 (IntBindingT t (ExceptT e2 (StateT s Identity))) a -> Either e2 (Either e1 a)
unwrapTypeMT cont = runIdentity . flip evalStateT cont . runExceptT . evalIntBindingT . runExceptT


fullTypeInfer cont = unwrapTypeMT cont . inferAndApply


-- Glueing things together and deconstructing the monad transformers.
fullTypeInferError cont term = case fullTypeInfer cont term of
  Left msg -> Left msg
  Right (Left unifError) -> Left (show unifError)
  Right (Right ty) -> Right ty

fullSchemeInferError :: 
    TypeEnv IntVar
    -> Term' a
    -> Either String (Scheme IntVar)
fullSchemeInferError cont term = case fullTypeInferError cont term of
    Left msg -> Left msg
    Right ty -> Right $ fullyAbstractType ty

-- fullSchemeInferFunctionError :: 
--     => TypeEnv IntVar
--     -> FunDef' a 
--     -> Either String (Scheme IntVar)
-- fullSchemeInferFunctionError cont (Function _ name body) = fullSchemeInferError cont body

fullPrettyTypeInfer cont term = case fullTypeInferError cont term of 
    Left msg -> msg 
    Right ty -> prettyPrintTypeFull ty


-- Pretty printing stuff

-- We assume an ample supply of variable names is given.
fullInstantiateWith :: Scheme a -> [a] -> UTerm TypeFormal a
fullInstantiateWith (Forall n scheme) vars =
    let neededVars = take n vars in -- :: [a]
    let absFun = \n -> UVar (neededVars !! n) in -- Int -> Identity a
    let theTy = instantiate absFun scheme in
    theTy

{-
This is just a default supply of string names to make a type look prettier and canonical.  In particular it's the stream 
  a,b,...,z,a0,b0...,a1,b1,...,a2,b2,....,
-}
defaultStringVarSupply :: [String]
defaultStringVarSupply =
    stringAtoZ ++
    [a ++ show n | n <- [0 ..] , a <- stringAtoZ]


stringAtoZ :: [String]
stringAtoZ = map return ['a' .. 'z']

defaultTextVarSupply :: [Text]
defaultTextVarSupply = map Text.pack defaultStringVarSupply

defaultIntSupply :: [Int]
defaultIntSupply = [0 ..]

defaultIntVarSupply :: [IntVar]
defaultIntVarSupply = map IntVar defaultIntSupply

-- To prettify a type, fully abstract it and then fully instantiate with the default string var supply
prettifyTypeVars :: Show a => UTerm TypeFormal a -> UTerm TypeFormal String
prettifyTypeVars ty = fullInstantiateWith (fullyAbstractType ( fmap show ty )) defaultStringVarSupply

prettyPrintType :: UTerm TypeFormal String -> String
prettyPrintType ty = case ty of
    UVar s -> s
    UTerm tf -> case tf of
      R -> "R"
      TUnit -> "(" ++ "_"
      (TPair ut ut') -> "(" ++ " " ++ prettyPrintType ut ++ " " ++ "," ++ " " ++ prettyPrintType ut' ++ " " ++ ")"

prettyPrintTypeFull :: Show a => UTerm TypeFormal a -> String
prettyPrintTypeFull = prettyPrintType . prettifyTypeVars

-- projType = UTerm (TArr (UTerm (TPair (UVar (IntVar 0)) (UVar (IntVar 1)))) (UVar (IntVar 0)))

prettifySchemeStringWith :: Show a => Scheme a -> [a] -> String
prettifySchemeStringWith scheme vars = prettyPrintTypeFull $ fullInstantiateWith scheme vars

prettyPrintSchemeText :: Scheme Text -> String
prettyPrintSchemeText scheme = prettifySchemeStringWith scheme defaultTextVarSupply
prettyPrintSchemeString :: Scheme String -> String
prettyPrintSchemeString scheme = prettifySchemeStringWith scheme defaultStringVarSupply
prettyPrintSchemeInt :: Scheme Int -> String
prettyPrintSchemeInt scheme = prettifySchemeStringWith scheme defaultIntSupply
prettyPrintSchemeIntVar :: Scheme IntVar -> String
prettyPrintSchemeIntVar scheme = prettifySchemeStringWith scheme defaultIntVarSupply

