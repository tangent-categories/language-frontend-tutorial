# Tutorial-Frontend-Language

## Building
The following will build the package on MacOS, Windows via WSL2, and any GNU/Linux system.

As of October 9, 2021, we are using the LTS version of GHC for 
which we use the best versions of alex and happy that we can.
These versions are hard-coded in the stack.yaml extra-deps section,
so update the stack.yaml on newer releases.

### Initial Build
1. Install nix:
    `curl -L https://nixos.org/nix/install | sh`
    The command is taken from https://nixos.org/download.html#nix-quick-install
    Also you should follow the "Verify installation" check to ensure integrity
2. Run `stack build`
    As a first time build, it will fail because BNFC hasn't
    been run yet.  
3. Do the bnfc build
    `./nix-build.sh`
    This runs bnfc on the Language.cf file with the correct arguments,
    and moves the grammar-ambigs.txt to the project root directory.
    The script ends by running the stack build command, and builds the 
    project.
4. Run with stack
    `stack run`
    This will run the compiler.

### Build after changing Language.cf
1. Run `./nix-build.sh`
2. Run `stack run`

### Build after any changes that do not include Language.cf
1. Run `stack build`
2. Run `stack run`


### Notes/Known bugs
If stack run fails to correctly parse immediately after a build, 
just exit and run stack run again.  It seems that if stack run
is used to build the program some random characters are left in the 
buffer and the first parse fails.  
 
To fix this bug, we changed the Main.hs file to flush stdout before accepting input.  Needs testing to ensure the bug is 
actually gone.


