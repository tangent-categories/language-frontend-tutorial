#! /usr/bin/env nix-shell
#! nix-shell -i bash --pure -p haskellPackages.BNFC haskellPackages.happy

# This script fetches bnfc and happy from nix-packages, then
#   runs bnfc on the correct file with the correct options
#   then runs happy on the generated grammar and extracts the 
#       ambiguity report 
#   and then finally cleans up.
#   No stateful change is made to the users' system with this method.

# Rebuild the cf file with the correct options and then build the project
cd src
bnfc --haskell -d --text-token --functor Language.cf
# go into the Language folder
cd Language
# TODO: do a similar check to see if happy is installed
happy --info=grammar-ambigs.txt --outfile=grammar.out Par.y
# remove the grammar.out file; it's not needed
rm grammar.out
# leave the directory
cd ..
# we're now back in the src directory
# let's move the grammar-ambigs to the project root 
mv Language/grammar-ambigs.txt ../grammar-ambigs.txt

# Remove the unused file that also breaks the build
rm Language/Test.hs